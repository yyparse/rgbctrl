// rgb control for MSI RTX 2080 Super Gaming X Trio
// gcc -o rgbctrl rgbctrl.c -ludev

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <libudev.h>

#define VENDOR_ID 0x10de
#define DEVICE_ID 0x1e81
#define SUBSYSTEM_VENDOR_ID 0x1462
#define SUBSYSTEM_DEVICE_ID 0xc724

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

#define PARSE_PARAM(var, max, name) \
	do { \
		if (argc <= pos) { \
			printf("parameter " name " missing\n"); \
			return 0; \
		} \
		var = strtoul(argv[pos], NULL, 10); \
		if (var > max) { \
			printf(name " not in valid range 0-%u\n", max); \
			return 0; \
		} \
		pos++; \
	} while (0)

#define CHK(x) \
	do { \
		if (x != 0) { \
			fprintf(stderr, "write failed\n"); \
			close(fd); \
			return 1; \
		} \
	} while (0)

#define FLAG_BRIGHTNESS 0x01
#define FLAG_SPEED      0x02
#define FLAG_REG        0x04

#define MAX_BRIGHTNESS (ARRAY_SIZE(brightness_values) - 1)
#define MAX_SPEED (ARRAY_SIZE(speed_values) - 1)

enum rgb {
	RGB_MODE_NONE, // no color can be selected
	RGB_MODE_1, // choose one color
	RGB_MODE_2, // choose two colors
};

struct effect {
	char *name;
	unsigned char flags;
	enum rgb mode;
	unsigned char cmd;
};

unsigned char brightness_values[] = { 0x14, 0x28, 0x3c, 0x50, 0x64 }; // allowed brightness values
unsigned char speed_values[] = { 0x04, 0x02, 0x01 }; // allowed speed values

int test_device(struct udev *udev, struct udev_list_entry *i2c_devs_entry, char i2c_dev[100])
{
	char *str = "NVIDIA i2c adapter 1 ";
	int ret = -1;
	const char *path;
	struct udev_device *dev;
	const char *devnode;
	const char *kernel;
	const char *name;
	struct udev_device *pdev;
	struct udev_device *ppdev;
	const char *device, *vendor;
	const char *subsystem_device, *subsystem_vendor;

	path = udev_list_entry_get_name(i2c_devs_entry);
	if (path == NULL)
		return ret;
	dev = udev_device_new_from_syspath(udev, path);
	if (dev == NULL)
		return ret;

	devnode = udev_device_get_devnode(dev);
	if (devnode == NULL)
		goto fail;
	kernel = udev_device_get_sysname(dev);
	if (kernel == NULL)
		goto fail;
	name = udev_device_get_sysattr_value(dev, "name");
	if (name == NULL)
		goto fail;

	if (strncmp(name, str, strlen(str)) != 0)
		goto fail;

	pdev = udev_device_get_parent(dev);
	if (pdev == NULL)
		goto fail;
	ppdev = udev_device_get_parent(pdev);
	if (ppdev == NULL)
		goto fail;

	vendor = udev_device_get_sysattr_value(ppdev, "vendor");
	device = udev_device_get_sysattr_value(ppdev, "device");
	subsystem_vendor = udev_device_get_sysattr_value(ppdev, "subsystem_vendor");
	subsystem_device = udev_device_get_sysattr_value(ppdev, "subsystem_device");
	if (vendor == NULL ||
	    device == NULL ||
	    subsystem_vendor == NULL ||
	    subsystem_device == NULL)
		goto fail;

	if (strtoul(vendor, NULL, 16) != VENDOR_ID ||
	    strtoul(device, NULL, 16) != DEVICE_ID ||
	    strtoul(subsystem_vendor, NULL, 16) != SUBSYSTEM_VENDOR_ID ||
	    strtoul(subsystem_device, NULL, 16) != SUBSYSTEM_DEVICE_ID)
		goto fail;

	snprintf(i2c_dev, 100, "%s", devnode);
	ret = 0;

fail:
	udev_device_unref(dev);
	return ret;
}

int detect(char i2c_dev[100])
{
	int ret = -1;
	struct udev *udev = NULL;
	struct udev_enumerate *udev_enumerate;
	struct udev_list_entry *i2c_devs, *i2c_devs_entry;
	char *res = NULL;
	
	udev = udev_new();
	if (udev == NULL)
		return ret;

	udev_enumerate = udev_enumerate_new(udev);
	if (udev_enumerate == NULL)
		goto fail_1;
	
	if (udev_enumerate_add_match_subsystem(udev_enumerate, "i2c-dev") < 0)
		goto fail_2;
	if (udev_enumerate_scan_devices(udev_enumerate) < 0)
		goto fail_2;
	
	i2c_devs = udev_enumerate_get_list_entry(udev_enumerate);
	if (i2c_devs == NULL)
		goto fail_2;

	udev_list_entry_foreach(i2c_devs_entry, i2c_devs) {
		if (test_device(udev, i2c_devs_entry, i2c_dev) == 0) {
			ret = 0;
			break;
		}
	}

fail_2:
	udev_enumerate_unref(udev_enumerate);
fail_1:
	udev_unref(udev);
	return ret;
}

int write_reg(int fd, uint8_t dev, uint8_t addr, unsigned char byte)
{
	struct i2c_smbus_ioctl_data args;
	union i2c_smbus_data data;

	bzero(&args, sizeof(args));
	bzero(&data, sizeof(data));
	
	if (ioctl(fd, I2C_SLAVE, dev) == -1)
		return -1;

	data.byte = byte;

	args.read_write = I2C_SMBUS_WRITE;
	args.command = addr;
	args.size = I2C_SMBUS_BYTE_DATA;
	args.data = &data;

	if (ioctl(fd, I2C_SMBUS, &args) == -1)
		return -1;

	return 0;
}

struct effect effect_list[] = {
	{
		.name = "off",
		.flags = 0,
		.mode = RGB_MODE_NONE,
		.cmd = 0x01
	},
	{
		.name = "rainbow",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_NONE,
		.cmd = 0x08
	},
	{
		.name = "static",
		.flags = FLAG_BRIGHTNESS,
		.mode = RGB_MODE_1,
		.cmd = 0x13
	},
	{
		.name = "raindrop",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x1a
	},
	{
		.name = "magic",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED,
		.mode = RGB_MODE_NONE,
		.cmd = 0x07
	},
	{
		.name = "patrolling",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x05
	},
	{
		.name = "streaming",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_NONE,
		.cmd = 0x06
	},
	{
		.name = "lightning",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED,
		.mode = RGB_MODE_NONE,
		.cmd = 0x15
	},
	{
		.name = "wave",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x1f
	},
	{
		.name = "meteor",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x16
	},
	{
		.name = "stack",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x0d
	},
	{
		.name = "rhythm",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x0b
	},
	{
		.name = "flowing",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x09
	},
	{
		.name = "whirling",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x0f
	},
	{
		.name = "twisting",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x11
	},
	{
		.name = "laminating",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED | FLAG_REG,
		.mode = RGB_MODE_1,
		.cmd = 0x1d
	},
	{
		.name = "fadein",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED,
		.mode = RGB_MODE_2,
		.cmd = 0x14
	},
	{
		.name = "breathing",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED,
		.mode = RGB_MODE_1,
		.cmd = 0x04
	},
	{
		.name = "flashing",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED,
		.mode = RGB_MODE_1,
		.cmd = 0x02
	},
	{
		.name = "doubleflashing",
		.flags = FLAG_BRIGHTNESS | FLAG_SPEED,
		.mode = RGB_MODE_1,
		.cmd = 0x03
	},
};

int cmd(struct effect *effect, int argc, char *argv[])
{
	int fd;
	char dev[100];
	unsigned long brightness, speed, r1, g1, b1, r2, g2, b2;
	int pos = 0;

	// 1. brightness
	if (effect->flags & FLAG_BRIGHTNESS)
		PARSE_PARAM(brightness, MAX_BRIGHTNESS, "brightness");
	// 2. speed
	if (effect->flags & FLAG_SPEED)
		PARSE_PARAM(speed, MAX_SPEED, "speed");
	// 3. color
	if (effect->mode == RGB_MODE_1) {
		PARSE_PARAM(r1, 255, "r");
		PARSE_PARAM(g1, 255, "g");
		PARSE_PARAM(b1, 255, "b");
	} else if (effect->mode == RGB_MODE_2) {
		PARSE_PARAM(r1, 255, "r1");
		PARSE_PARAM(g1, 255, "g1");
		PARSE_PARAM(b1, 255, "b1");
		PARSE_PARAM(r2, 255, "r2");
		PARSE_PARAM(g2, 255, "g2");
		PARSE_PARAM(b2, 255, "b2");
	}

	if (detect(dev) != 0) {
		fprintf(stderr, "MSI RTX 2080 Super Gaming X Trio not found\n");
		return 1;
	}

	fd = open(dev, O_RDWR);
	if (fd == -1) {
		fprintf(stderr, "could not open device\n");
		return 1;
	}
	if (effect->flags & FLAG_BRIGHTNESS)
		CHK(write_reg(fd, 0x68, 0x36, brightness_values[brightness]));
	if (effect->flags & FLAG_SPEED)
		CHK(write_reg(fd, 0x68, 0x38, speed_values[speed]));
	if (effect->flags & FLAG_REG)
		CHK(write_reg(fd, 0x68, 0x26, 0x00));
	if (effect->mode == RGB_MODE_1) {
		CHK(write_reg(fd, 0x68, 0x30, r1));
		CHK(write_reg(fd, 0x68, 0x31, g1));
		CHK(write_reg(fd, 0x68, 0x32, b1));
	} else if (effect->mode == RGB_MODE_2) {
		CHK(write_reg(fd, 0x68, 0x27, r1));
		CHK(write_reg(fd, 0x68, 0x28, g1));
		CHK(write_reg(fd, 0x68, 0x29, b1));
		CHK(write_reg(fd, 0x68, 0x2a, r2));
		CHK(write_reg(fd, 0x68, 0x2b, g2));
		CHK(write_reg(fd, 0x68, 0x2c, b2));
	}
	CHK(write_reg(fd, 0x68, 0x22, effect->cmd));
	CHK(write_reg(fd, 0x68, 0x3f, 0x01));
	close(fd);
	return 0;
}

void print_usage(char *prog)
{
	size_t i;
	
	printf("Usage: %s <effect> ...\n", prog);
	
	for (i = 0; i < ARRAY_SIZE(effect_list); i++) {
		printf("%s", prog);
		printf(" %s", effect_list[i].name);
		// 1. brightness
		if (effect_list[i].flags & FLAG_BRIGHTNESS)
			printf(" <brightness>");
		// 2. speed
		if (effect_list[i].flags & FLAG_SPEED)
			printf(" <speed>");
		// 3. color
		if (effect_list[i].mode == RGB_MODE_1)
			printf(" <r> <g> <b>");
		else if (effect_list[i].mode == RGB_MODE_2)
			printf(" <r1> <g1> <b1> <r2> <g2> <b2>");
		printf("\n");
	}

	printf("brightness: 0 - %u\n", MAX_BRIGHTNESS);
	printf("speed: 0 - %u\n", MAX_SPEED);
	printf("r: 0 - 255\n");
	printf("g: 0 - 255\n");
	printf("b: 0 - 255\n");
}

int main(int argc, char *argv[])
{
	struct effect *effect;
	size_t i;

	if (argc <= 1) {
		print_usage(argc > 0 ? argv[0] : "");
		return 0;
	}

	effect = NULL;
	for (i = 0; i < ARRAY_SIZE(effect_list); i++) {
		if (strcmp(argv[1], effect_list[i].name) == 0) {
			effect = &effect_list[i];
			break;
		}
	}
	if (effect == NULL) {
		fprintf(stderr, "unknown effect\n");
		return 1;
	}

	return cmd(effect, argc - 2, argv + 2);
}
