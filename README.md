RGB control for MSI RTX 2080 Super Gaming X Trio

## Usage examples

```sh-session
# ./rgbctrl rainbow 3 2
# ./rgbctrl off
# ./rgbctrl static 3 255 255 0
# ./rgbctrl lightning 4 2
# ./rgbctrl fadein 3 2 255 0 0 255 255 255
```
